/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Eduction
 */
package lljv;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import jpa.entities.Accounts;
import jpa.entities.FabAccount;
import jpa.entities.LeaveDistribution;

/**
 *
 * @author dab66
 */
@Named
public class Employee {

    private static jpa.session.EmployeeFacade employeeFacade;
    private static jpa.session.LeaveDistributionFacade leaveDistributionFacade;
    private static jpa.session.WbsFacade wbsFacade;

    /**
     * wbsHours = hash showing the hours user worked on each wbs. key = wbs,
     * value = hours
     */
    private Hashtable<String, Double> wbsHours = new Hashtable<>();
    /**
     * localAccountPercent = hash showing the percentage of time to be charged
     * to each accountString. key = accountString, value = percent
     */
    private Hashtable<String, Double> accountPercent = new Hashtable<>();
    // key = account, value = percent for use in leave distribution
    private Hashtable<String, Double> leaveDistribution = new Hashtable<>();
    private Integer employeeID;
    private String repliconName;
    private Integer position;
    private Integer objectCode;
    private Double semimonthly;
    private static final Double FRINGE = .349;
    private String leaveExists;
    private Double totalHours;
    private Double hoursOff;
    // hoursWorked = toalHours - hoursOff
    private Double hoursWorked;
    // String representing leave distrubtion used in this report
    private static String leaveDistributionExport;
    // Number of pay periods to use for calculating leave distribution
    private static final Integer MAXHISTORYUSED = 4;
    // number of historical pay periods used for this employee
    private Integer historyUsed;
    // total amount charged in KFS report
    private Double totalCharged;
    // total period eranings from KFS report
    private Double earnings;
    // total period benefits from KFS report
    private Double benefits;
    private static EntityManager em;

    public Employee() {

    }

    public Employee(jpa.session.EmployeeFacade employeeFacade, jpa.session.LeaveDistributionFacade leaveDistributionFacade, jpa.session.WbsFacade wbsFacade, EntityManager em) {
        Employee.employeeFacade = employeeFacade;
        Employee.leaveDistributionFacade = leaveDistributionFacade;
        Employee.wbsFacade = wbsFacade;
        Employee.leaveDistributionExport = "";
        Employee.em = em;

    }

    public Employee(Integer employeeID) {
        this.totalHours = 0.0;
        this.hoursOff = 0.0;
        this.historyUsed = 0;
        this.totalCharged = 0.0;
        this.earnings = 0.0;
        this.benefits = 0.0;
        this.employeeID = employeeID;
        this.repliconName = getNameFromDB();
        this.leaveExists = "";
    }

    // find this employee id in DB, and set the name
    private String getNameFromDB() {
        String name = "";
        jpa.entities.Employee employeeEntity = this.employeeFacade.find(this.employeeID);
        if (employeeEntity == null) {
            LLJV.addError(this.employeeID + " not found in database.  Ignoring " + this.employeeID);
        } else {
            name = employeeEntity.getRepliconName();
        }
        if (LLJV.DEBUG) {
            System.out.println(name);
        }
        return name;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public void setObjectCode(Integer objectCode) {
        this.objectCode = objectCode;
    }

    public void setSemimonthly(Double semimonthly) {
        this.semimonthly = semimonthly;
    }

    public void addHours(Double hours) {
        this.totalHours += hours;
    }

    public void addWBS(String wbs, Double hours) {
        // check if wbs has already been added, and update or add
        if (wbsHours.get(wbs) != null) {
            wbsHours.put(wbs, wbsHours.get(wbs) + hours);
        } else {
            wbsHours.put(wbs, hours);
        }
    }

    // charge all accounts that make up wbs's for this employee
    public void chargeAccounts() {
        this.hoursWorked = this.totalHours;
        // check to make sure percent adds up to 1
        Double totalPercent = 0.0;
        Double percentForLeave = 0.0;
        if (setAccountPercent()) {
            // loop through every account / percent this employee charged time to to create leave_distribution
            int index = 0;
            purgeLeaveDistribution();
            for (String accountString : this.accountPercent.keySet()) {
                index++;
                Double percent = accountPercent.get(accountString);
                totalPercent += percent;
                // split accountString into account and subaccount
                String[] accountSplit = accountString.split("#");
                String account = accountSplit[0];
                String subAccount = "";
                if (accountSplit.length > 1) {
                    subAccount = accountSplit[1];
                }
                // percent of time worked on that account, not including time off
                Double percentWorked = percent * (this.totalHours / this.hoursWorked);
                percentForLeave += percentWorked;
                if (index == this.accountPercent.size() && percentForLeave != 1.0) {
                    Double correction = 1.0 - percentForLeave;
                    percentWorked += correction;
                    LLJV.addWarning(this.repliconName + "\'s (" + this.employeeID + ") total leave_distribution percent is " + percentForLeave + ".\n\tCorrecting by adding " + correction + " to their entry into our leave_distribution database for " + account + " " + subAccount);
                    percentForLeave += correction;
                }
                System.out.println(this.employeeID + " total leave_distribution percent for " + account + "#" + subAccount + " = " + percentWorked);
                createLeaveDistribution(account, subAccount, percentWorked);
            }
            Double percentOff = 1.0 - totalPercent;
            if (percentOff != 0.0) {
                chargeTimeOff(percentOff);
            }
            // check to make sure amount charged equals debits from KFS report
            Double charged = 0.0;
            // check to make sure earnings charged equals earnings from KFS report
            Double earningsCharge = 0.0;
            // check to make sure benefits charged equals benefits from KFS report
            Double benefitsCharge = 0.0;
            // loop through localAccountPercent and add debits to CSV
            index = 0;
            for (String accountString : this.accountPercent.keySet()) {
                index++;
                Double percent = this.accountPercent.get(accountString);
                // split accountString into account and subaccount
                String[] accountSplit = accountString.split("#");
                String account = accountSplit[0];
                String subAccount = "";
                if (accountSplit.length > 1) {
                    subAccount = accountSplit[1];
                }
                Double amount = percent * this.semimonthly;
                Double fringe = FRINGE * amount;
                charged += amount;
                charged += fringe;
                earningsCharge += amount;
                benefitsCharge += fringe;
                if ((index == this.accountPercent.size()) && charged != totalCharged) {
                    Double correction = totalCharged - charged;
                    percent += correction;
                    LLJV.addWarning(this.repliconName + "\'s (" + this.employeeID + ") total debits = " + charged + " instead of " + totalCharged
                            + ".\n\tTotal earnings debits = " + earningsCharge + ", while earnings credits were " + this.earnings
                            + "\n\tTotal benefits debits = " + benefitsCharge + ", while benefits credits were " + this.benefits
                            + "\n\tCorrecting by adding " + correction + " to their fringe debit line in our CSV for " + account + " " + subAccount);
                    fringe += correction;
                }
                Query query = em.createNamedQuery("Accounts.findByAccountAndSubAccount");
                query.setParameter("account", account);
                query.setParameter("subAccount", subAccount);
                List<Accounts> accountsResult = query.getResultList();
                if (!accountsResult.get(0).getActive()) {
                    LLJV.addError(this.repliconName + " (" + this.employeeID + ") charged time to an inactive account. \n\t Remove salary and fringe debit lines for " + account + "#" + subAccount + " from CSV.");
                }
                if (accountsResult.get(0).getFlagged()) {
                    LLJV.addError(this.repliconName + " (" + this.employeeID + ") charged time to a flagged account. \n\t Check salary and fringe debit lines for " + account + "#" + subAccount + " from CSV.");
                }

                LLJVCSV.addDebit(account, subAccount, this.objectCode, this.position, this.employeeID, amount);
                LLJVCSV.addFringe(account, subAccount, this.position, this.employeeID, fringe);
            }
            if (LLJV.DEBUG) {
                System.out.println("total hours for " + this.repliconName + " = " + this.totalHours);
            }
        }

    }

    // check and remove if leave distribution already exists for this employee and pay period
    public void purgeLeaveDistribution() {
        List<jpa.entities.LeaveDistribution> leaveDistributions = Employee.leaveDistributionFacade.findAll();
        for (jpa.entities.LeaveDistribution leaveDistribution : leaveDistributions) {
            Date leaveDate = leaveDistribution.getLeaveDistributionPK().getPayPeriodDate();
            if ((leaveDistribution.getLeaveDistributionPK().getEmployeeId() == this.employeeID) && (leaveDate.equals(LLJVCSV.getMysqlDate()))) {
                if (this.leaveExists.equals("")) {
                    this.leaveExists = "Deleting existing leave distribution data for " + this.repliconName + " (" + this.employeeID + ") for pay period ending " + LLJVCSV.getPayPeriodEndDate();
                    LLJV.addNotice(this.leaveExists);
                }
                Employee.leaveDistributionFacade.remove(leaveDistribution);
            }
        }

    }

    public void createLeaveDistribution(String account, String subAccount, Double percent) {
        // convert to non-fabrication account if needed
        Query query = em.createNamedQuery("FabAccount.findByAccountAndSubAccount");
        query.setParameter("account", account);
        query.setParameter("subAccount", subAccount);
        List<FabAccount> result = query.getResultList();
        if (result.size() > 0 && result.get(0) != null) {
            FabAccount fabAcc = result.get(0);
            Accounts nonFabAcc = fabAcc.getNonFab();
            account = nonFabAcc.getAccountsPK().getAccount();
            subAccount = nonFabAcc.getAccountsPK().getSubAccount();
            if (LLJV.DEBUG) {
                System.out.println(account + "#" + subAccount);
            }
        }
        // check if already exists in DB from initial charge to non-fab account
        query = em.createNamedQuery("LeaveDistribution.findByEmployeeIdAndDateAndAccountAndSubAccount");
        query.setParameter("employeeId", this.employeeID);
        query.setParameter("payPeriodDate", LLJVCSV.getMysqlDate());
        query.setParameter("account", account);
        query.setParameter("subAccount", subAccount);
        List<LeaveDistribution> ldResult = query.getResultList();
        if (ldResult.size() > 0) {
            LeaveDistribution ld = ldResult.get(0);
            // TODO: This introduces a rounding error, possibly seen later in findRecentLeaveDistributions
            percent += ld.getPercent();
            Employee.leaveDistributionFacade.remove(ld);
        }

        // enter into leave distribution
        jpa.entities.LeaveDistributionPK leaveDistributionPK = new jpa.entities.LeaveDistributionPK(this.employeeID, LLJVCSV.getMysqlDate(), account, subAccount);
        jpa.entities.LeaveDistribution ld = new jpa.entities.LeaveDistribution(leaveDistributionPK, percent);
        if (LLJV.DEBUG) {
            System.out.println(ld.getLeaveDistributionPK().getPayPeriodDate());
            System.out.println("\t" + ld.getEmployee());
            System.out.println("\t" + ld.getLeaveDistributionPK().getAccount());
            System.out.println("\t" + ld.getLeaveDistributionPK().getSubAccount());
            System.out.println("\t" + ld.getPercent());
        }
        Employee.leaveDistributionFacade.create(ld);
    }

    // set the localAccountPercent hash
    // return true if successful, false if not
    private Boolean setAccountPercent() {
        if (this.wbsHours.size() == 0) {
            LLJV.addError(this.repliconName + " (" + this.employeeID + ") was in the KFS report but not in the Replicon report. \n\t For consistency, manually remove the credit line for " + this.employeeID + " from the CSV report.");
            return false;
        } else {
            // loop through each WBS this employee charged time to
            for (String wbs_number : this.wbsHours.keySet()) {
                // add up time off
                if (wbs_number.equals("off")) {
                    this.hoursOff += this.wbsHours.get(wbs_number);
                    this.hoursWorked -= this.hoursOff;
                    LLJV.addInfo(this.repliconName + " (" + this.employeeID + ") took " + this.hoursOff + " hours off.");
                } // don't count non lepp /chess
                /**
                 * else if (wbs_number.equals("Non_Lepp-Chess")) { Double
                 * nonCLASSE = this.wbsHours.get(wbs_number); this.hoursWorked
                 * -= nonCLASSE; LLJV.addWarning(this.repliconName + " (" +
                 * this.employeeID + ") charged " + nonCLASSE + " hours to " +
                 * wbs_number + "."); }
                 *
                 */
                else {
                    WBS wbs = new WBS(wbs_number);
                    // check for inactive or flagged wbs numbers
                    jpa.entities.Wbs wbsEntity = wbsFacade.find(wbs_number);
                    if (!wbsEntity.getActive()) {
                        LLJV.addError(this.repliconName + " (" + this.employeeID + ") charged " + this.wbsHours.get(wbs_number) + " hours to an inactive WBS: " + wbs_number);
                    }
                    if (wbsEntity.getFlagged()) {
                        LLJV.addError(this.repliconName + " (" + this.employeeID + ") charged " + this.wbsHours.get(wbs_number) + " hours to a flagged WBS: " + wbs_number);
                    }

                    // key = wbs number, value = percent of hours reported
                    Hashtable<String, Double> wbsAccounts = wbs.charge(this.wbsHours.get(wbs_number) / this.totalHours);
                    // key = wbs number, value = percent of hours worked (not off)
                    Hashtable<String, Double> wbsWorked = wbs.charge(this.wbsHours.get(wbs_number) / this.hoursWorked);
                    // loop through each account for this wbs
                    for (String account : wbsAccounts.keySet()) {
                        Double percent = wbsAccounts.get(account);
                        // add to overall localAccountPercent listing for this employee
                        if (this.accountPercent.get(account) != null) {
                            this.accountPercent.put(account, this.accountPercent.get(account) + percent);
                        } else {
                            this.accountPercent.put(account, percent);
                        }
                    }
                }
            }
            return true;
        }
    }

    // charge time off based on historical data in leave_distribution
    private void chargeTimeOff(Double percent) {
        // hash of every account user reported to in previous pay periods
        // key = account, value = totalPercentNormalized percent
        Hashtable<String, Double> accountsCharged = new Hashtable<>();
        Double totalPercent = 0.0;
        Double totalPercentNormalized = 0.0;
        leaveDistributionExport += this.repliconName + "\n";
        // build accountsCharged
        List<LeaveDistribution> leaveDistributions = findRecentLeaveDistributions(this.employeeID);
        for (LeaveDistribution leaveDistribution : leaveDistributions) {
            String account = leaveDistribution.getLeaveDistributionPK().getAccount() + "#" + leaveDistribution.getLeaveDistributionPK().getSubAccount();
            Double accountPercent = leaveDistribution.getPercent();
            totalPercent += accountPercent;
            leaveDistributionExport += "\t" + account + " " + accountPercent + " " + formatDate(leaveDistribution.getLeaveDistributionPK().getPayPeriodDate()) + "\n";
            if (accountsCharged.containsKey(account)) {
                accountsCharged.put(account, accountsCharged.get(account) + accountPercent);

            } else {
                accountsCharged.put(account, accountPercent);
            }

        }
        // add accountsCharged to overall localAccountPercent
        for (String account : accountsCharged.keySet()) {
            Double localAccountPercent = (accountsCharged.get(account) / totalPercent) * percent;
            totalPercentNormalized += localAccountPercent;
            // add to overall localAccountPercent listing for this employee
            if (this.accountPercent.containsKey(account)) {
                this.accountPercent.put(account, this.accountPercent.get(account) + localAccountPercent);
            } else {
                this.accountPercent.put(account, localAccountPercent);
            }
        }
        if (LLJV.DEBUG) {
            System.out.println("Leave distribution for " + this.repliconName + " totals " + totalPercent + ".  Normalized, = " + totalPercentNormalized + ".  Percent = " + percent);
        }
    }

    public void setName(String repliconName) {
        this.repliconName = repliconName;
    }

    public String getName() {
        return this.repliconName;
    }

    public Integer getID() {
        return this.employeeID;
    }

    public void downloadLeave() throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        ec.responseReset();
        String contentType = "text/plain";
        ec.setResponseContentType(contentType);
        String fileName = "leave_distributions.txt";
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

        OutputStream output = ec.getResponseOutputStream();
        try (Writer w = new OutputStreamWriter(output, "UTF-8")) {
            w.write(leaveDistributionExport);
        }

        fc.responseComplete();

    }

    public String formatDate(Date date) {
        // convert between date format of KFS report and date formate in MySQL
        DateFormat kfsFormat = new SimpleDateFormat("MM/dd/yy");
        DateFormat mysqlFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date kfsDate = null;
        String formattedDate = kfsFormat.format(date);
        return formattedDate;
    }

    public List<LeaveDistribution> findRecentLeaveDistributions(Integer employeeID) {
        ArrayList<Date> dates = new ArrayList<>();
        ArrayList<jpa.entities.LeaveDistribution> leaveDistributions = new ArrayList<>();

        Query query = em.createNamedQuery("LeaveDistribution.findByEmployeeIdOrderedByDate");
        query.setParameter("employeeId", employeeID);
        List<LeaveDistribution> result = query.getResultList();

        for (LeaveDistribution ld : result) {
            Date date = ld.getLeaveDistributionPK().getPayPeriodDate();
            if (!dates.contains(date) && (dates.size() < MAXHISTORYUSED)) {
                historyUsed++;
                dates.add(date);
                if (LLJV.DEBUG) {
                    System.out.println(this.employeeID + " " + date);
                }
            }
        }
        for (Date date : dates) {
            // check if leave distribution for that date equals 1.0
            Double leaveCheck = 0.0;
            query = em.createNamedQuery("LeaveDistribution.findByEmployeeIdAndDate");
            query.setParameter("employeeId", employeeID);
            query.setParameter("payPeriodDate", date);
            result = query.getResultList();
            for (LeaveDistribution ld : result) {
                leaveCheck += ld.getPercent();
                if (LLJV.DEBUG) {
                    System.out.println(ld.getLeaveDistributionPK().getEmployeeId() + " " + ld.getLeaveDistributionPK().getAccount() + " " + ld.getLeaveDistributionPK().getSubAccount() + " " + ld.getPercent() + " " + ld.getLeaveDistributionPK().getPayPeriodDate());
                }
                String account = ld.getLeaveDistributionPK().getAccount();
                String subAccount = ld.getLeaveDistributionPK().getSubAccount();
                query = em.createNamedQuery("Accounts.findByAccountAndSubAccount");
                query.setParameter("account", account);
                query.setParameter("subAccount", subAccount);
                List<Accounts> accountsResult = query.getResultList();
                if (accountsResult.get(0).getActive()) {
                    leaveDistributions.add(ld);
                }
                System.out.println(this.repliconName + "\" (" + this.employeeID + ") on " + formatDate(date) + " for " + account + "#" + subAccount + " totals " + ld.getPercent());
            }
            //TODO: There will likely be rounding errors if fabrication accounts were used
            //  These can be ignored in general, as we normalze the percent in chargeTimeOff to account for inactive accounts.
            if (LLJV.DEBUG) {
                if (leaveCheck != 1.0) {
                    LLJV.addWarning("historical leave for \"" + this.repliconName + "\" (" + this.employeeID + ") on " + formatDate(date) + " totals " + leaveCheck + " instead of 1.0.");
                }
            }
        }

        return leaveDistributions;
    }

    // return amount charged in KFS report
    public Double getCharged() {
        return this.totalCharged;
    }

    // add a charge from the KFS report
    public void addCharge(Double charge) {
        this.totalCharged += charge;
    }

    // add an earnings charge
    public void addEarnings(Double earnings) {
        this.earnings += earnings;
    }

    // add a benefits charge
    public void addBenefits(Double benefits) {
        this.benefits += benefits;
    }
}
