/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package lljv;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.Hashtable;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * WBS class to represent and manage each WBS
 *
 * @author dab66
 */
@Named
public class WBS implements Serializable {

    private static jpa.session.WbsBreakdownFacade wbsBreakdownFacade;
    private static jpa.session.WbsFacade wbsFacade;

    // A hash showing which accounts make up which percentage of this wbs. 
    //key = account#subaccount, value = percent
    private Hashtable<String, Double> accountPercent = new Hashtable<>();

    private String wbs;

    public WBS() {

    }

    public WBS(jpa.session.WbsFacade wbsFacade, jpa.session.WbsBreakdownFacade wbsBreakdownFacade) {
        WBS.wbsBreakdownFacade = wbsBreakdownFacade;
        WBS.wbsFacade = wbsFacade;
    }

    public WBS(String wbs) {
        Double breakdownTotal = 0.0;
        this.wbs = wbs;
        List<jpa.entities.WbsBreakdown> wbsBreakdowns = wbsBreakdownFacade.findAll();
        // loop through each WBS Breakdown
        for (jpa.entities.WbsBreakdown wbsBreakdown : wbsBreakdowns) {
            // if the breakdown is for this wbs, add to hash
            if (wbsBreakdown.getWbs().getWbsNumber().equals(wbs)) {
                Double percent = wbsBreakdown.getPercent();
                breakdownTotal += percent;
                String account = wbsBreakdown.getWbsBreakdownPK().getAccount() + "#" + wbsBreakdown.getWbsBreakdownPK().getSubAccount();
                accountPercent.put(account, percent);
            }
            if (LLJV.DEBUG) {
                System.out.println(wbsBreakdown.getWbsBreakdownPK().getAccount() + " " + wbsBreakdown.getWbsBreakdownPK().getSubAccount() + " " + wbsBreakdown.getPercent());
            }
        }
        if (breakdownTotal == 0.0) {
            LLJV.addError("No breakdown found for " + wbs);
        } else if (breakdownTotal != 1.0) {
            LLJV.addError("Breakdowns for " + wbs + " add up to " + breakdownTotal + " instead of 1.0");
        }
    }

    /**
     *
     * @param percent
     * @return hash showing which accounts should be charged what amount
     */
    public Hashtable<String, Double> charge(Double percent) {

        // key = account#subaccount, value = percent
        Hashtable<String, Double> charge = new Hashtable<>();
        // loop through accounts for this WBS, and calculate percent
        for (String account : accountPercent.keySet()) {
            charge.put(account, percent * accountPercent.get(account));

        }
        return charge;
    }

    // creat string representing all WBS breakdowns
    public String outputBreakdown() {
        String breakdown = "";
        List<jpa.entities.Wbs> wbss = wbsFacade.findAll();
        List<jpa.entities.WbsBreakdown> wbsBreakdowns = wbsBreakdownFacade.findAll();
        // loop through each wbs
        for (jpa.entities.Wbs wbs : wbss) {
            // check to make sure add up to 1.0
            Double check = 0.0;
            breakdown += wbs.getWbsNumber() + "\n";
            // find each breakdown for that wbs
            for (jpa.entities.WbsBreakdown wbsBreakdown : wbsBreakdowns) {
                if (wbsBreakdown.getWbs().equals(wbs)) {
                    breakdown += "\t" + wbsBreakdown.getWbsBreakdownPK().getAccount() + " " + wbsBreakdown.getWbsBreakdownPK().getSubAccount() + " " + wbsBreakdown.getPercent() + "\n";
                    check += wbsBreakdown.getPercent();
                }
            }
            if (check != 1.0) {
                LLJV.addError("Breakdown for " + wbs + " equals " + check + " instead of 1.0.");
            }

        }

        return breakdown;
    }

    public void downloadBreakdown() throws IOException {
        String breakdown = outputBreakdown();
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        ec.responseReset();
        String contentType = "text/plain";
        ec.setResponseContentType(contentType);
        String fileName = "WBS_Breakdown.txt";
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

        OutputStream output = ec.getResponseOutputStream();
        try (Writer w = new OutputStreamWriter(output, "UTF-8")) {
            w.write(breakdown);
        }

        fc.responseComplete();
    }

}
