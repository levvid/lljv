/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Eduction
 */
package lljv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * User class representing the logged-in use.
 *
 * @author dab66
 */
public class User {

    public User() {
        username = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getHeader("remote_user");
        //username = "dab66";
    }

    public String getUsername() {
        return username;
    }

    private String username;

    public static HttpServletRequest getRequest() {
        HttpServletRequest request
                = (HttpServletRequest) FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getRequest();
        if (request == null) {
            throw new RuntimeException("Sorry. Got a null request from faces context");
        }
        return request;
    }

    public static Object getAttribute(String name) {
        HttpServletRequest request = getRequest();
        return request.getAttribute(name);

    }

    public Boolean isAdmin() throws IOException, InterruptedException {
        StringBuffer output = new StringBuffer();
        String command = "id -G -n " + username;

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String groups = reader.readLine();
            String[] grouplist = groups.split(" ");

            String search = "busoff";
            for (String str : grouplist) {
                if (str.trim().contains(search)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

}
