/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based Sciences and Education
 */
package lljv;

import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * The main LLJV
 *
 * @author dab66
 */
@Named
public class LLJV {

    private ArrayList<Employee> employees;
    private static String returnInfo;
    private static String returnNotice;
    private static String returnWarning;
    private static String returnError;
    private static ArrayList<String> notInKFS;
    public static final Boolean DEBUG = false;
    private KFS kfs;
    private Replicon replicon;
    private User user;
    private Employee employee;
    private WBS wbs;
    @EJB
    private jpa.session.EmployeeFacade employeeFacade;
    @EJB
    private jpa.session.WbsBreakdownFacade wbsBreakdownFacade;
    @EJB
    private jpa.session.WbsFacade wbsFacade;
    @EJB
    private jpa.session.LeaveDistributionFacade leaveDistributionFacade;
    @PersistenceContext(name = "LLJVPU")
    private EntityManager em;
    private LLJVCSV lljvcsv;

    public String LLJV() {
        em.getEntityManagerFactory().getCache().evictAll();
        lljvcsv = new LLJVCSV();
        this.employees = new ArrayList<>();
        returnNotice = "";
        returnError = "";
        returnWarning = "";
        returnInfo = "";
        notInKFS = new ArrayList<>();
        // pass static facades to employee class
        this.employee = new Employee(employeeFacade, leaveDistributionFacade, wbsFacade, em);
        // pass static facades to wbs class
        this.wbs = new WBS(wbsFacade, wbsBreakdownFacade);
        this.user = new User();
        this.employees = KFS.parseCSV(employees);
        this.employees = Replicon.parseCSV(employees);
        chargeAccounts();
        em.getEntityManagerFactory().getCache().evictAll();
        return "lljv/results";
    }

    public static void addNotice(String message) {
        returnNotice += "NOTICE: " + message + "\n";
    }

    public static void addError(String message) {
        returnError += "ERROR: " + message + "\n";
    }

    public String getReturnNotice() {
        return returnNotice;
    }

    public String getReturnError() {
        return returnError;
    }

    public static void addInfo(String message) {
        returnInfo += "INFO: " + message + "\n";
    }

    public String getReturnInfo() {
        return returnInfo;
    }

    public static void addWarning(String message) {
        returnWarning += "WARNING: " + message + "\n";
    }

    public String getReturnWarning() {
        return returnWarning;
    }

    public static void addNotInKFS(String name) {
        if (!notInKFS.contains(name)) {
            addError(name + " was found in replicon report, but not in KFS");
            notInKFS.add(name);
        }
    }

    // loop through employees list
    public void chargeAccounts() {
        if (this.employees != null) {
            for (Employee employee : this.employees) {
                employee.chargeAccounts();
            }
        }
    }
}
