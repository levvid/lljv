/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package lljv;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * Object to represent generated CSV that will be uploaded to KFS
 *
 * @author dab66
 */
@Named
@SessionScoped
public class LLJVCSV implements Serializable {

    private static String csv;
    private static Integer fiscalYear;
    private static Integer payPeriod;
    private static String payPeriodEndDate;
    private static String runID;
    private static final Integer FRINGEOBJECTCODE = 5600;
    private static Date mysqlDate;
    private static Boolean twoDates;

    public LLJVCSV() {
        LLJVCSV.csv = "Chart,Account,Sub-Acct,Object,,,,,Position,Empl ID,,Earn Code,,,,Run ID,Pay Period End Date,Pay FY,Pay Per,,,,,,,,,,Debit,Credit\n";
        twoDates = false;
        payPeriodEndDate = "";
    }

    public static void setPayPeriodEndDate(String pay_period_end_date) {
        if ((LLJVCSV.payPeriodEndDate != null) && (LLJVCSV.payPeriodEndDate != "")) {

            if (!LLJVCSV.payPeriodEndDate.equals(pay_period_end_date) && !twoDates) {
                twoDates = true;
                LLJV.addError("KFS Report contains two pay period end dates.  It must contain only one.");
            }

        }
        LLJVCSV.payPeriodEndDate = pay_period_end_date;
        if (LLJVCSV.payPeriodEndDate != "" ) {
            setMysqlDate();
        }
    }

    public static void setRunID(String runid) {
        LLJVCSV.runID = runid;
    }

    public static void addCredit(String account, String sub_account, Integer object_code, Integer position, Integer employee_id, Double credit) {
        LLJVCSV.csv += "IT," + account + "," + sub_account + "," + object_code + ",,,,," + position + "," + employee_id + ",,SAL,,,," + LLJVCSV.runID + "," + LLJVCSV.payPeriodEndDate + "," + LLJVCSV.fiscalYear + "," + LLJVCSV.payPeriod + ",,,,,,,,,,," + credit + "\n";

    }

    public static void addDebit(String account, String sub_account, Integer object_code, Integer position, Integer employee_id, Double debit) {
        LLJVCSV.csv += "IT," + account + "," + sub_account + "," + object_code + ",,,,," + position + "," + employee_id + ",,SAL,,,," + LLJVCSV.runID + "," + LLJVCSV.payPeriodEndDate + "," + LLJVCSV.fiscalYear + "," + LLJVCSV.payPeriod + ",,,,,,,,,," + debit + ",\n";

    }

    public static void addFringe(String account, String sub_account, Integer position, Integer employee_id, Double debit) {
        LLJVCSV.csv += "IT," + account + "," + sub_account + "," + LLJVCSV.FRINGEOBJECTCODE + ",,,,," + position + "," + employee_id + ",,SAL,,,," + LLJVCSV.runID + "," + LLJVCSV.payPeriodEndDate + "," + LLJVCSV.fiscalYear + "," + LLJVCSV.payPeriod + ",,,,,,,,,," + debit + ",\n";

    }

    public void download() throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        ec.responseReset();
        String contentType = "text/csv";
        ec.setResponseContentType(contentType);
        String fileName = "CLASSE_LLJV-" + this.fiscalYear + "-" + this.payPeriod + ".csv";
        ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

        OutputStream output = ec.getResponseOutputStream();
        Writer w = new OutputStreamWriter(output, "UTF-8");
        w.write(this.csv);
        w.close();

        fc.responseComplete();
    }

    public Integer getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(Integer fiscalYear) {
        LLJVCSV.fiscalYear = fiscalYear;
    }

    public Integer getPayPeriod() {
        return payPeriod;
    }

    public void setPayPeriod(Integer payPeriod) {
        LLJVCSV.payPeriod = payPeriod;
    }

    public static String getPayPeriodEndDate() {
        return payPeriodEndDate;
    }

    public String getCsv() {
        return csv;
    }

    public void validateCSV(FacesContext ctx, UIComponent comp, Object value) {
        List<FacesMessage> msgs = new ArrayList<>();
        Part file = (Part) value;
        /**
         * if (file.getSize() > 1024) { msgs.add(new FacesMessage("file too
         * big")); }*
         */
        if (!"text/csv".equals(file.getContentType())) {
            msgs.add(new FacesMessage("not a csv file"));
        }
        if (!msgs.isEmpty()) {
            throw new ValidatorException(msgs);
        }
    }

    public static void setMysqlDate() {
        // convert between date format of KFS report and date formate in MySQL
        DateFormat kfsFormat = new SimpleDateFormat("MM/dd/yy");
        DateFormat mysqlFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date kfsDate = null;
        try {
            kfsDate = kfsFormat.parse(LLJVCSV.payPeriodEndDate);
        } catch (ParseException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        String mysqlDateString = mysqlFormat.format(kfsDate);
        try {
            LLJVCSV.mysqlDate = mysqlFormat.parse(mysqlDateString);
        } catch (ParseException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (LLJV.DEBUG) {
            System.out.println(mysqlDate);
        }
    }

    public static Date getMysqlDate() {
        return LLJVCSV.mysqlDate;
    }

}
