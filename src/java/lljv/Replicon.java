/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package lljv;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * Class with methods to manage replicon report
 *
 * @author dab66
 */
@Named
@SessionScoped
public class Replicon implements Serializable {

    private Part repliconCsv;
    private static String repliconContent;
    private String message;

    // parse CSV, updating employees list
    public static ArrayList<Employee> parseCSV(ArrayList<Employee> employees) {
        // loop through each line of replicon report
        try (Scanner scanner = new Scanner(repliconContent)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                // names are enclosed in quotes
                String[] nameSplit = line.split("\"");
                String wbs;
                // make sure line contained name (discard blank lines)
                if (nameSplit.length > 2) {
                    // namesplit is in format:
                    // "lastname, firstname", wbs, hours
                    String[] wbsSplit = nameSplit[2].split(",");
                    String name = nameSplit[1];
                    Double hours = Double.parseDouble(wbsSplit[4]);
                    if (wbsSplit[3].equals("")) {
                        wbs = "off";
                    } else {
                        wbs = wbsSplit[3];
                    }
                    if (LLJV.DEBUG) {
                        System.out.println("name: " + name + " hours: " + hours + " wbs: " + wbs);
                    }
                    Employee employee = null;
                    // find employee in employees list, and update
                    if (employees != null) {
                        for (Employee e : employees) {
                            if (e.getName().equals(name)) {
                                employee = e;
                                employee.addHours(hours);
                                employee.addWBS(wbs, hours);
                                employees.set(employees.indexOf(e), employee);
                            }
                        }
                    }
                    if (employee == null) {
                        LLJV.addNotInKFS(name);
                    }
                }
            }
            return employees;
        }
    }

    public void uploadCSV() {
        try {
            repliconContent = new Scanner(repliconCsv.getInputStream()).useDelimiter("\\A").next();
        } catch (IOException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "error uploading Replicon CSV " + repliconCsv.getSubmittedFileName(),
                    null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        setMessage("Successfully uploaded " + repliconCsv.getSubmittedFileName());

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void clearMessage() {
        this.message = "";
    }

    public Part getRepliconCsv() {
        return repliconCsv;
    }

    /**
     *
     * @param repliconCsv
     */
    public void setRepliconCsv(Part repliconCsv) {
        this.repliconCsv = repliconCsv;
    }

    public String getRepliconContent() {
        return repliconContent;
    }

}
