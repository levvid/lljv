/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package lljv;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * Object and methods for managing KFS report
 *
 * @author dab66
 */
@Named
@SessionScoped
public class KFS implements Serializable {

    // actual CSV file
    private Part kfsCsv;
    // string representation of contents of CSV
    private static String kfsContent;
    // message for JSF web page
    private String message;

    // parse the CSV, and return a list of employees in the CSV
    public static ArrayList<Employee> parseCSV(ArrayList<Employee> employees) {
        // loop through each line of the csv
        try (Scanner scanner = new Scanner(kfsContent)) {
            while (scanner.hasNextLine()) {
                String[] line = scanner.nextLine().split(",");
                // discard everything except "Normal Payrool Activity"
                if (line[9].equals("Labor - Normal Payroll Activity")) {
                    String account = line[0].split(" ")[0];
                    String subAccount = line[1].split(" ")[0];
                    Integer employeeID = Integer.parseInt(line[4]);
                    Integer position = Integer.parseInt(line[5]);
                    Double comp = Double.parseDouble(line[7]);
                    String payPeriodEndDate = line[12];
                    String runID = line[13];
                    Integer objectCode = Integer.parseInt(line[15].split(" ")[0]);
                    // amount is in column 20 for earnings, 21 for benefits
                    Double credit, earnings, benefits = 0.0;
                    earnings = Double.parseDouble(line[20]);
                    credit = earnings;
                    if (earnings == 0) {
                        benefits = Double.parseDouble(line[21]);
                        credit = benefits;
                    }
                    if (LLJV.DEBUG) {
                        System.out.println("account: " + account + " subAccount: " + subAccount + " objectCode: " + objectCode + " position: " + position + " ID: " + employeeID + " credit: " + credit);
                    }
                    LLJVCSV.setPayPeriodEndDate(payPeriodEndDate);
                    LLJVCSV.setRunID(runID);
                    Employee employee = null;
                    // loop through employees to see if object already exists
                    if (employees != null) {
                        for (Employee e : employees) {
                            if (Objects.equals(e.getID(), employeeID)) {
                                employee = e;
                                // add credit for the debits found in KFS CSV
                                LLJVCSV.addCredit(account, subAccount, objectCode, position, employeeID, credit);
                                employee.addCharge(credit);
                                employee.addEarnings(earnings);
                                employee.addBenefits(benefits);
                            }
                        }
                    }
                    // if employee wasn't already in employees list, add it
                    if (employee == null) {
                        employee = new Employee(employeeID);
                        if (employee.getName() != "") {
                            employee.setObjectCode(objectCode);
                            employee.setSemimonthly(comp / 24);
                            employee.setPosition(position);
                            employees.add(employee);
                            // add credit for the debits found in KFS CSV
                            LLJVCSV.addCredit(account, subAccount, objectCode, position, employeeID, credit);
                            employee.addCharge(credit);
                            employee.addEarnings(earnings);
                            employee.addBenefits(benefits);

                        }
                    }
                }
            }
            return employees;
        }
    }

    public void uploadCSV() {
        kfsContent = "";
        LLJVCSV.setPayPeriodEndDate("");
        try {
            kfsContent = new Scanner(kfsCsv.getInputStream()).useDelimiter("\\A").next();
        } catch (IOException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "error uploading KFS CSV " + kfsCsv.getSubmittedFileName(),
                    null);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        if (multipleDates()) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    kfsCsv.getSubmittedFileName() + " contains more than one pay period end date.",
                    null);
            FacesContext.getCurrentInstance().addMessage(null, msg);

        } else {
            setMessage("Successfully uploaded " + kfsCsv.getSubmittedFileName());

        }
    }

    // make sure only one pay period end date in CSV
    public Boolean multipleDates() {
        // loop through each line of the csv
        try (Scanner scanner = new Scanner(kfsContent)) {
            while (scanner.hasNextLine()) {
                String[] line = scanner.nextLine().split(",");
                // discard everything except "Normal Payrool Activity"
                if (line[9].equals("Labor - Normal Payroll Activity")) {
                    String payPeriodEndDate = line[12];
                    if ((LLJVCSV.getPayPeriodEndDate() != null) && (LLJVCSV.getPayPeriodEndDate() != "")) {

                        if (!LLJVCSV.getPayPeriodEndDate().equals(payPeriodEndDate)) {
                            return true;
                        }
                    }
                    LLJVCSV.setPayPeriodEndDate(payPeriodEndDate);
                }
            }
        }
        return false;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void clearMessage() {
        this.message = "";
    }

    public Part getKfsCsv() {
        return kfsCsv;
    }

    public void setKfsCsv(Part kfsCsv) {
        this.kfsCsv = kfsCsv;
    }

    public String getKfsContent() {
        return kfsContent;
    }

}
