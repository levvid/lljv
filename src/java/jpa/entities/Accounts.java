/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based Sciences and Education
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dab66
 */
@Entity
@Table(name = "accounts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Accounts.findAll", query = "SELECT a FROM Accounts a"),
    @NamedQuery(name = "Accounts.findByAccount", query = "SELECT a FROM Accounts a WHERE a.accountsPK.account = :account"),
    @NamedQuery(name = "Accounts.findByAccountAndSubAccount", query = "SELECT a FROM Accounts a WHERE a.accountsPK.account = :account AND a.accountsPK.subAccount = :subAccount"),
    @NamedQuery(name = "Accounts.findBySubAccount", query = "SELECT a FROM Accounts a WHERE a.accountsPK.subAccount = :subAccount"),
    @NamedQuery(name = "Accounts.findByActive", query = "SELECT a FROM Accounts a WHERE a.active = :active")})
public class Accounts implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "flagged")
    private boolean flagged = false;

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AccountsPK accountsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active = true;

    public Accounts() {
    }

    public Accounts(AccountsPK accountsPK) {
        this.accountsPK = accountsPK;
    }

    public Accounts(AccountsPK accountsPK, boolean active) {
        this.accountsPK = accountsPK;
        this.active = active;
    }

    public Accounts(String account, String subAccount) {
        this.accountsPK = new AccountsPK(account, subAccount);
    }

    public AccountsPK getAccountsPK() {
        return accountsPK;
    }

    public void setAccountsPK(AccountsPK accountsPK) {
        this.accountsPK = accountsPK;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountsPK != null ? accountsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accounts)) {
            return false;
        }
        Accounts other = (Accounts) object;
        if ((this.accountsPK == null && other.accountsPK != null) || (this.accountsPK != null && !this.accountsPK.equals(other.accountsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return accountsPK.toString();
    }

    public boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

}
