/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "leave_distribution")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LeaveDistribution.findAll", query = "SELECT l FROM LeaveDistribution l"),
    @NamedQuery(name = "LeaveDistribution.findByEmployeeId", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.employeeId = :employeeId"),
    @NamedQuery(name = "LeaveDistribution.findByEmployeeIdOrderedByDate", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.employeeId = :employeeId ORDER BY l.leaveDistributionPK.payPeriodDate DESC"),
    @NamedQuery(name = "LeaveDistribution.findByEmployeeIdAndDate", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.employeeId = :employeeId AND l.leaveDistributionPK.payPeriodDate = :payPeriodDate"),
    @NamedQuery(name = "LeaveDistribution.findByEmployeeIdAndDateAndAccountAndSubAccount", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.employeeId = :employeeId AND l.leaveDistributionPK.payPeriodDate = :payPeriodDate AND l.leaveDistributionPK.account = :account AND l.leaveDistributionPK.subAccount = :subAccount"),
    @NamedQuery(name = "LeaveDistribution.findByPercent", query = "SELECT l FROM LeaveDistribution l WHERE l.percent = :percent"),
    @NamedQuery(name = "LeaveDistribution.findByPayPeriodDate", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.payPeriodDate = :payPeriodDate"),
    @NamedQuery(name = "LeaveDistribution.findByAccount", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.account = :account"),
    @NamedQuery(name = "LeaveDistribution.findBySubAccount", query = "SELECT l FROM LeaveDistribution l WHERE l.leaveDistributionPK.subAccount = :subAccount")})
public class LeaveDistribution implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LeaveDistributionPK leaveDistributionPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "percent")
    private Double percent;
    @JoinColumns({
        @JoinColumn(name = "account", referencedColumnName = "account", insertable = false, updatable = false),
        @JoinColumn(name = "sub_account", referencedColumnName = "sub_account", insertable = false, updatable = false),})
    @ManyToOne(optional = false)
    private Accounts accounts;
    @JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Employee employee;

    public LeaveDistribution() {
    }

    public LeaveDistribution(LeaveDistributionPK leaveDistributionPK) {
        this.leaveDistributionPK = leaveDistributionPK;
    }

    public LeaveDistribution(LeaveDistributionPK leaveDistributionPK, Double percent) {
        this.leaveDistributionPK = leaveDistributionPK;
        this.percent = percent;
    }

    public LeaveDistribution(int employeeId, Date payPeriodDate, String account, String subAccount) {
        this.leaveDistributionPK = new LeaveDistributionPK(employeeId, payPeriodDate, account, subAccount);
    }
    public LeaveDistributionPK getLeaveDistributionPK() {
        return leaveDistributionPK;
    }

    public void setLeaveDistributionPK(LeaveDistributionPK leaveDistributionPK) {
        this.leaveDistributionPK = leaveDistributionPK;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (leaveDistributionPK != null ? leaveDistributionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeaveDistribution)) {
            return false;
        }
        LeaveDistribution other = (LeaveDistribution) object;
        if ((this.leaveDistributionPK == null && other.leaveDistributionPK != null) || (this.leaveDistributionPK != null && !this.leaveDistributionPK.equals(other.leaveDistributionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.LeaveDistribution[ leaveDistributionPK=" + leaveDistributionPK + " ]";
    }

}
