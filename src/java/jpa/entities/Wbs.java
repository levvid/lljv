/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gibson Levvid
 */
@Entity
@Table(name = "wbs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Wbs.findAll", query = "SELECT w FROM Wbs w"),
    @NamedQuery(name = "Wbs.findByWbsNumber", query = "SELECT w FROM Wbs w WHERE w.wbsNumber = :wbsNumber")})
public class Wbs implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active = true;
    @Basic(optional = false)
    @NotNull
    @Column(name = "flagged")
    private boolean flagged = false;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "wbs_number")
    private String wbsNumber;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wbs")
    private Collection<WbsBreakdown> wbsBreakdownCollection;

    public Wbs() {
    }

    public Wbs(String wbsNumber) {
        this.wbsNumber = wbsNumber;
    }

    public String getWbsNumber() {
        return wbsNumber;
    }

    public void setWbsNumber(String wbsNumber) {
        this.wbsNumber = wbsNumber;
    }

    @XmlTransient
    public Collection<WbsBreakdown> getWbsBreakdownCollection() {
        return wbsBreakdownCollection;
    }

    public void setWbsBreakdownCollection(Collection<WbsBreakdown> wbsBreakdownCollection) {
        this.wbsBreakdownCollection = wbsBreakdownCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wbsNumber != null ? wbsNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Wbs)) {
            return false;
        }
        Wbs other = (Wbs) object;
        if ((this.wbsNumber == null && other.wbsNumber != null) || (this.wbsNumber != null && !this.wbsNumber.equals(other.wbsNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return wbsNumber;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }
    
}
