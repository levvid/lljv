/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author dab66
 */
@Embeddable
public class FabAccountPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "account")
    private String account;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "sub_account")
    private String subAccount = "";

    public FabAccountPK() {
    }

    public FabAccountPK(String account, String subAccount) {
        this.account = account;
        this.subAccount = subAccount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (account != null ? account.hashCode() : 0);
        hash += (subAccount != null ? subAccount.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FabAccountPK)) {
            return false;
        }
        FabAccountPK other = (FabAccountPK) object;
        if ((this.account == null && other.account != null) || (this.account != null && !this.account.equals(other.account))) {
            return false;
        }
        if ((this.subAccount == null && other.subAccount != null) || (this.subAccount != null && !this.subAccount.equals(other.subAccount))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.FabAccountPK[ account=" + account + ", subAccount=" + subAccount + " ]";
    }
    
}
