/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dab66
 */
@Entity
@Table(name = "fab_account")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FabAccount.findAll", query = "SELECT f FROM FabAccount f"),
    @NamedQuery(name = "FabAccount.findByAccount", query = "SELECT f FROM FabAccount f WHERE f.fabAccountPK.account = :account"),
    @NamedQuery(name = "FabAccount.findByAccountAndSubAccount", query = "SELECT f FROM FabAccount f WHERE f.fabAccountPK.account = :account AND f.fabAccountPK.subAccount = :subAccount"),
    @NamedQuery(name = "FabAccount.findBySubAccount", query = "SELECT f FROM FabAccount f WHERE f.fabAccountPK.subAccount = :subAccount")})
public class FabAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FabAccountPK fabAccountPK;
    @JoinColumns({
        @JoinColumn(name = "account", referencedColumnName = "account", insertable = false, updatable = false),
        @JoinColumn(name = "sub_account", referencedColumnName = "sub_account", insertable = false, updatable = false)})
    @OneToOne(optional = false)
    private Accounts accounts;
    @JoinColumns({
        @JoinColumn(name = "nonfab_account", referencedColumnName = "account"),
        @JoinColumn(name = "nonfab_subaccount", referencedColumnName = "sub_account")})
    @ManyToOne
    private Accounts nonFab;

    public FabAccount() {
    }

    public FabAccount(FabAccountPK fabAccountPK) {
        this.fabAccountPK = fabAccountPK;
    }

    public FabAccount(String account, String subAccount) {
        this.fabAccountPK = new FabAccountPK(account, subAccount);
    }

    public FabAccountPK getFabAccountPK() {
        return fabAccountPK;
    }

    public void setFabAccountPK(FabAccountPK fabAccountPK) {
        this.fabAccountPK = fabAccountPK;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Accounts getNonFab() {
        return nonFab;
    }

    public void setNonFab(Accounts nonFab) {
        this.nonFab = nonFab;
    }

    public String getNonFabAccount() {
        System.out.println("NonFabAccount = " + this.nonFab.accountsPK.getAccount());
        return this.nonFab.accountsPK.getAccount();
    }

    public String getNonFabSubAccount() {
        return this.nonFab.accountsPK.getSubAccount();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fabAccountPK != null ? fabAccountPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FabAccount)) {
            return false;
        }
        FabAccount other = (FabAccount) object;
        if ((this.fabAccountPK == null && other.fabAccountPK != null) || (this.fabAccountPK != null && !this.fabAccountPK.equals(other.fabAccountPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpa.entities.FabAccount[ fabAccountPK=" + fabAccountPK + " ]";
    }

}
