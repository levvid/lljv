/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.WbsBreakdown;

/**
 *
 * @author dab66
 */
@Stateless
public class WbsBreakdownFacade extends AbstractFacade<WbsBreakdown> {

    @PersistenceContext(unitName = "LLJVPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WbsBreakdownFacade() {
        super(WbsBreakdown.class);
    }
    
}
