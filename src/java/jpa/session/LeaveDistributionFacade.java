/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.LeaveDistribution;

/**
 *
 * @author dab66
 */
@Stateless
public class LeaveDistributionFacade extends AbstractFacade<LeaveDistribution> {

    @PersistenceContext(unitName = "LLJVPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LeaveDistributionFacade() {
        super(LeaveDistribution.class);
    }
    
}
