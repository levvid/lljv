/*
 * Property of CLASSE
 * Cornell Laboratory for Accelerator-based ScienceS and Education
 */
package jpa.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jpa.entities.FabAccount;

/**
 *
 * @author dab66
 */
@Stateless
public class FabAccountFacade extends AbstractFacade<FabAccount> {

    @PersistenceContext(unitName = "LLJVPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FabAccountFacade() {
        super(FabAccount.class);
    }
    
}
